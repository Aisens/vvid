#include "test.h"

void test::test_quadraticEquration(){
    double XpowTWO = 1.0;
    double XpowONE = 3.0;
    double XpowZERO = 2.0;
    double * resultAnswer = new double[3];
    resultAnswer[0] = 1;
    resultAnswer[1] = -1;
    resultAnswer[2] = -2;
    double * resultFunction = quadraticEquration(XpowTWO, XpowONE, XpowZERO);

    for (int i = 0 ; i < 3 ; i++)
    QCOMPARE(resultAnswer[i], resultFunction[i]);

}
