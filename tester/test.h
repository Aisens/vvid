#ifndef TEST_H
#define TEST_H

#include <QtCore>
#include <QtTest/QTest>
#include <QtTest>
#include "../func.h"


class test : public QObject
{
    Q_OBJECT

public:

private slots:
    void test_arctg();
    void test_cos();
    void test_exponenta();
    void test_func();
    void test_ln();
    void test_sin();
    void test_quadraticEquration();
    void test_factorial();
    void test_mysqrt();
};


#endif // TEST_H

