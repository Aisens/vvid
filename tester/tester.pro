QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  mainm.cpp \
            ../mysqrt.cpp \
            ../factorial.cpp \
            ../arctg.cpp \
            test_arctg.cpp \
            ../cos.cpp \
            test_cos.cpp \
            ../exponenta.cpp \
            test_exponenta.cpp \
            ../func.cpp \
            test_func.cpp \
            ../ln.cpp \
            test_ln.cpp \
            ../sin.cpp \
            test_sin.cpp \
            ../quadraticequration.cpp \
            test_quadraticequration.cpp \
    test_factorial.cpp \
    test_mysqrt.cpp

HEADERS += \
    test.h \
    ..\func.h
