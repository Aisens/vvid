#include "test.h"

void test::test_cos()
{
    double e = 0.1;
    double x = 1;
    QCOMPARE(0.5, cos(x, e));

    double ee = 0.01;
    double xx = 0.4;
    QCOMPARE(0.92, cos(xx, ee));


}
