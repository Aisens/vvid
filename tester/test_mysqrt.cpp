#include "test.h"

void test::test_mysqrt()
{
    double e = 4.0;
    QCOMPARE(2.0, mySqrt(e));
    e = 9.0;
    QCOMPARE(3.0, mySqrt(e));
    e = 16.0;
    QCOMPARE(4.0, mySqrt(e));
}
