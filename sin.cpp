#include "func.h"

double sin(double x, double accuracy){
    double result = x;
    double xxx = x*x*x;
    double f = 3;
    double minus = -1;
    do
    {
        result = result + (minus) * xxx/fact(f);
        minus *= -1;
        xxx = xxx * x * x;
        f += 2;
    }while(fabs(xxx/fact(f)) >= accuracy);
    return result;
}
