#include "func.h"

double exponenta(double x, double accuracy)
{
    double result = 1; // результат (1 потому что это первое значение ряда)
    double pow = x;
    int b = 0;
    do
    {
        b++;
        result = result + (x / fact(b));
        x = x * pow;
    }while(fabs(x/fact(b)) >= accuracy);
    return result;
}
