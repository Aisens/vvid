/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGroupBox *groupBox;
    QLabel *label;
    QDoubleSpinBox *Vx;
    QLabel *label_3;
    QLabel *label_4;
    QSpinBox *Va;
    QDoubleSpinBox *Vacc;
    QGroupBox *groupBox_2;
    QRadioButton *rb_e;
    QRadioButton *rb_sin;
    QRadioButton *rb_cos;
    QRadioButton *rb_ln;
    QRadioButton *rb_oneplusxpowa;
    QRadioButton *rb_arctg;
    QGroupBox *groupBox_3;
    QTextBrowser *textBrowser;
    QFrame *line;
    QGroupBox *groupBox_4;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QTextBrowser *Discriminant_TextBrowser;
    QFrame *line_2;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_2;
    QTextBrowser *Xone_TextBrowser;
    QTextBrowser *Xtwo_TextBrowser;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QDoubleSpinBox *XpowTWO;
    QLabel *label_7;
    QDoubleSpinBox *XpowONE;
    QLabel *label_8;
    QDoubleSpinBox *XpowZERO;
    QLabel *label_9;
    QPushButton *pushButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(541, 291);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(10, 80, 111, 201));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(10, 20, 61, 16));
        Vx = new QDoubleSpinBox(groupBox);
        Vx->setObjectName(QString::fromUtf8("Vx"));
        Vx->setGeometry(QRect(10, 40, 91, 21));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(10, 80, 61, 16));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(10, 140, 47, 13));
        Va = new QSpinBox(groupBox);
        Va->setObjectName(QString::fromUtf8("Va"));
        Va->setGeometry(QRect(10, 100, 91, 22));
        Vacc = new QDoubleSpinBox(groupBox);
        Vacc->setObjectName(QString::fromUtf8("Vacc"));
        Vacc->setGeometry(QRect(10, 160, 91, 21));
        Vacc->setDecimals(6);
        Vacc->setMinimum(0.000000000000000);
        Vacc->setMaximum(100.000000000000000);
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(130, 80, 131, 201));
        rb_e = new QRadioButton(groupBox_2);
        rb_e->setObjectName(QString::fromUtf8("rb_e"));
        rb_e->setGeometry(QRect(10, 20, 82, 17));
        rb_sin = new QRadioButton(groupBox_2);
        rb_sin->setObjectName(QString::fromUtf8("rb_sin"));
        rb_sin->setGeometry(QRect(10, 50, 82, 17));
        rb_cos = new QRadioButton(groupBox_2);
        rb_cos->setObjectName(QString::fromUtf8("rb_cos"));
        rb_cos->setGeometry(QRect(10, 80, 82, 17));
        rb_ln = new QRadioButton(groupBox_2);
        rb_ln->setObjectName(QString::fromUtf8("rb_ln"));
        rb_ln->setGeometry(QRect(10, 110, 82, 17));
        rb_oneplusxpowa = new QRadioButton(groupBox_2);
        rb_oneplusxpowa->setObjectName(QString::fromUtf8("rb_oneplusxpowa"));
        rb_oneplusxpowa->setGeometry(QRect(10, 140, 82, 17));
        rb_arctg = new QRadioButton(groupBox_2);
        rb_arctg->setObjectName(QString::fromUtf8("rb_arctg"));
        rb_arctg->setGeometry(QRect(10, 170, 82, 17));
        groupBox_3 = new QGroupBox(centralWidget);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(10, 10, 251, 61));
        textBrowser = new QTextBrowser(groupBox_3);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(10, 20, 231, 31));
        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(250, -10, 41, 321));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        groupBox_4 = new QGroupBox(centralWidget);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(280, 10, 251, 91));
        gridLayoutWidget = new QWidget(groupBox_4);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 20, 231, 61));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        Discriminant_TextBrowser = new QTextBrowser(gridLayoutWidget);
        Discriminant_TextBrowser->setObjectName(QString::fromUtf8("Discriminant_TextBrowser"));

        gridLayout->addWidget(Discriminant_TextBrowser, 0, 1, 2, 1);

        line_2 = new QFrame(gridLayoutWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);

        gridLayout->addWidget(line_2, 0, 2, 2, 1);

        label_6 = new QLabel(gridLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout->addWidget(label_6, 1, 3, 1, 1);

        label_5 = new QLabel(gridLayoutWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout->addWidget(label_5, 0, 3, 1, 1);

        label_2 = new QLabel(gridLayoutWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout->addWidget(label_2, 0, 0, 2, 1);

        Xone_TextBrowser = new QTextBrowser(gridLayoutWidget);
        Xone_TextBrowser->setObjectName(QString::fromUtf8("Xone_TextBrowser"));

        gridLayout->addWidget(Xone_TextBrowser, 0, 4, 1, 1);

        Xtwo_TextBrowser = new QTextBrowser(gridLayoutWidget);
        Xtwo_TextBrowser->setObjectName(QString::fromUtf8("Xtwo_TextBrowser"));

        gridLayout->addWidget(Xtwo_TextBrowser, 1, 4, 1, 1);

        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(290, 110, 231, 31));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        XpowTWO = new QDoubleSpinBox(horizontalLayoutWidget);
        XpowTWO->setObjectName(QString::fromUtf8("XpowTWO"));

        horizontalLayout->addWidget(XpowTWO);

        label_7 = new QLabel(horizontalLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout->addWidget(label_7);

        XpowONE = new QDoubleSpinBox(horizontalLayoutWidget);
        XpowONE->setObjectName(QString::fromUtf8("XpowONE"));

        horizontalLayout->addWidget(XpowONE);

        label_8 = new QLabel(horizontalLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout->addWidget(label_8);

        XpowZERO = new QDoubleSpinBox(horizontalLayoutWidget);
        XpowZERO->setObjectName(QString::fromUtf8("XpowZERO"));

        horizontalLayout->addWidget(XpowZERO);

        label_9 = new QLabel(horizontalLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout->addWidget(label_9);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(450, 150, 75, 23));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        groupBox->setTitle(QApplication::translate("MainWindow", "\320\222\320\262\320\276\320\264 \320\264\320\260\320\275\320\275\321\213\321\205", nullptr));
        label->setText(QApplication::translate("MainWindow", "\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265 x", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "\320\227\320\275\320\260\321\207\320\265\320\275\320\270\320\265 a", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "\320\242\320\276\321\207\320\275\320\276\321\201\321\202\321\214", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "\320\235\320\260\321\205\320\276\320\266\320\264\320\265\320\275\320\270\320\265 \321\204\321\203\320\275\320\272\321\206\320\270\320\270", nullptr));
        rb_e->setText(QApplication::translate("MainWindow", "e^x", nullptr));
        rb_sin->setText(QApplication::translate("MainWindow", "sin(x)", nullptr));
        rb_cos->setText(QApplication::translate("MainWindow", "cos(x)", nullptr));
        rb_ln->setText(QApplication::translate("MainWindow", "ln(1+x)", nullptr));
        rb_oneplusxpowa->setText(QApplication::translate("MainWindow", "(1+x)^a", nullptr));
        rb_arctg->setText(QApplication::translate("MainWindow", "arctg(x)", nullptr));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202", nullptr));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "\320\240\320\265\320\267\321\203\320\273\321\214\321\202\320\260\321\202", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "X2 = ", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "X1 = ", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "D = ", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "X^2 +", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "X +", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "= 0", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\320\240\320\265\321\210\320\270\321\202\321\214", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
