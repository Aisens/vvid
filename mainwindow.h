#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_rb_e_clicked();

    void on_rb_cos_clicked();

    void on_rb_sin_clicked();

    void on_rb_arctg_clicked();

    void on_rb_ln_clicked();

    void on_rb_oneplusxpowa_clicked();

    //void on_pushButton_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
