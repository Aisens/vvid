#include "func.h"

double mySqrt(double number)
{
    double error = 0.000000001;
    double s = number;

    while ((s - number / s) > error)
    {
        s = (s + number / s) / 2;
    }
    return s;
}
