#ifndef FUNC_H
#define FUNC_H
#include <cmath>

/*!
\file
\brief Функция для нахождения арктангенса с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является параметром функции arctg(x)
\param accuracy - Число double, является точностью
\return Функция возвращает число double, результат arctg(x)
*/
double arctg(double x, double accuracy);

/*!
\file
\brief Функция для нахождения косинуса с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является параметром функции cos(x)
\param accuracy - Число double, является точностью
\return Функция возвращает число double, результат cos(x)
*/
double cos(double x, double accuracy);

/*!
\file
\brief Функция для нахождения экспоненты в степени x с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является степенью экспоненты
\param accuracy - Число double, является точностью
\return Функция возвращает число double, результат e^x
*/
double exponenta(double x, double accuracy);

/*!
\file
\brief Функция для нахождения функции (1+x)^a с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является параметром x в функции (1+x)^a
\param accuracy - Число double, является точностью
\param a - Число double, является степенью
\return Функция возвращает число double, результат (1+x)^a
*/
double func(double x, double accuracy, double a);

/*!
\file
\brief Функция для нахождения логарифма ln(1+x) с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является параметром x функции ln(1+x)
\param accuracy - Число double, является точностью
\return Функция возвращает число double, результат ln(1+x)
*/
double ln(double x, double accuracy);

/*!
\file
\brief Функция для нахождения синуса с помощью ряда Тейлора
\author Strokan Andrey
\date 10.06.2019
\param x - Число double, является параметром x функции sin(x)
\param accuracy - Число double, является точностью
\return Функция возвращает число double, результат sin(x)
*/
double sin(double x, double accuracy);

/*!
\file
\brief Функция для нахождения дискриминанта и корней квадратного уравнения
\author Strokan Andrey
\date 10.06.2019
\param a - Число double, является коэффициентом квадратного уравнения стоящим перед x^2
\param b - Число double, является коэффициентом квадратного уравнения стоящим перед x^1
\param c - Число double, является коэффициентом квадратного уравнения стоящим перед x^0
\return Функция возвращает массив из чисел double, в порядке: дискриминант, первый корень, второй корень
*/
double * quadraticEquration(double a, double b, double c);

/*!
\file
\brief Функция для нахождения корня от числа при помощи Вавилонского метода
\author Strokan Andrey
\date 10.06.2019
\param number - Число double, является числом которое нужно возвести в корень
\return Функция возвращает число double, корень от числа
*/
double mySqrt(double number);

/*!
\file
\brief Функция для нахождения факториала
\author Strokan Andrey
\date 10.06.2019
\param N - Число double, является числом факториал которого необходимо найти
\return Функция возвращает число double, факториал числа
*/
double fact(int N);

#endif // FUNC_H
