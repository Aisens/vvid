# Программа vvid
<ol>
    <li>
        <h2>Нужна для:</h2>
        <ol>
            <li>Нахождения синуса</li>
            <li>Нахождения косинуса</li>
            <li>Нахождения арктангенса</li>
            <li>Нахождения e^x</li>
            <li>Нахождения (1+x)^a</li>
            <li>Нахождения факториала</li>
            <li>Нахождения корня</li>
            <li>Нахождения дискриминанта, и корней уравнения</li>
            <li>Нахождения ln(1+x)</li>
        </ol>
    </li>
    <li>
        <h2>Хранит в себе функции:</h2>
        <ol>
            <li>double sin(double x, double accuracy);</li>
            <li>double cos(double x, double accuracy);</li>
            <li>double arctg(double x, double accuracy);</li>
            <li>double exponenta(double x, double accuracy);</li>
            <li>double func(double x, double accuracy, double a);</li>
            <li>double fact(int N);</li>
            <li>double mySqrt(double number);</li>
            <li>double * quadraticEquration(double a, double b, double c);</li>
            <li>double ln(double x, double accuracy);</li>
        </ol>
    </li>
</ol>
