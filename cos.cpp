#include "func.h"

double cos(double x, double accuracy){
    double result = 1;
    double znak = -1;
    double xx = x*x;
    double fuc = 2;
    do
    {
        result = result + znak*xx/fact(fuc);
        xx = xx*x*x;
        fuc += 2;
        znak = znak*(-1);
    }while(fabs(xx/fact(fuc)) >= accuracy);
    return result;
}
