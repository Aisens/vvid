#include "func.h"

/**
\ Функция принимает на вход параметр x в ln(1+x) и точность, далее цикл считает функцию по ряду Тейлора до тех пор пока текущее значение ряда не станет меньше чем точность
\ Функция возвращает результат ln(1+x)
*/
double ln(double x, double accuracy)
{
    double res = x;
    double minus = -1;
    double xx = x*x;
    double num = 2;
    do
    {
        res = res + (minus) * xx/num;
        xx = xx*x;
        num++;
        minus = minus*(-1);
    }while(fabs(xx/num) >= accuracy);
    return res;
}
