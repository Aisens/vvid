#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "func.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_rb_e_clicked() // кнопка вычисления e^x
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
       double result = exponenta(ui->Vx->value(), ui->Vacc->value());
       ui->textBrowser->setText(QString::number(result));
}

void MainWindow::on_rb_cos_clicked() // кнопка вычисления cos (x)
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
        double result = cos(ui->Vx->value(), ui->Vacc->value());
        ui->textBrowser->setText(QString::number(result));
}

void MainWindow::on_rb_sin_clicked()
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
     double result = sin(ui->Vx->value(), ui->Vacc->value());
     ui->textBrowser->setText(QString::number(result));
}

void MainWindow::on_rb_arctg_clicked() // Кнопка вычисления arctg(x)
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
        double result = arctg(ui->Vx->value(), ui->Vacc->value());
        ui->textBrowser->setText(QString::number(result));
}

void MainWindow::on_rb_ln_clicked()
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
       double result = ln(ui->Vx->value(), ui->Vacc->value());
       ui->textBrowser->setText(QString::number(result));
}

void MainWindow::on_rb_oneplusxpowa_clicked()
{
    if(ui->Vx->value() == 0.0 || ui->Vacc->value() == 0.0) return;
       double result = func(ui->Vx->value(), ui->Vacc->value(), ui->Va->value());
       ui->textBrowser->setText(QString::number(result));
}

//void MainWindow::on_pushButton_clicked()
//{
    //if(ui->XpowTWO->value() == 0.0 && ui->XpowO->value() == 0.0 && ui->XpowZERO->value() == 0.0) return;

//}

void MainWindow::on_pushButton_clicked()
{
    if((ui->XpowTWO->value() == 0.0 && ui->XpowONE->value() == 0.0 && ui->XpowZERO->value() == 0.0) || ui->XpowTWO->value() == 0.0) return;
    double *result = new double[3];
    result = quadraticEquration(ui->XpowTWO->value(), ui->XpowONE->value(), ui->XpowZERO->value());
    ui->Discriminant_TextBrowser->setText(QString::number(result[0]));
    if(result[0] > 0.0){
        ui->Xone_TextBrowser->setText(QString::number(result[1]));
        ui->Xtwo_TextBrowser->setText(QString::number(result[2]));
    }else if (result[0] == 0.0){
        ui->Xone_TextBrowser->setText(QString::number(result[1]));
        ui->Xtwo_TextBrowser->setText("Корня нет");
    }else{
        ui->Xone_TextBrowser->setText("Корня нет");
        ui->Xtwo_TextBrowser->setText("Корня нет");
    }
}
