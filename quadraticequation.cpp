#include "func.h"

double * quadraticEquration(double a, double b, double c){
    double Discriminant = 0.0;
    double Xone = 0.0;
    double Xtwo = 0.0;

    Discriminant = b*b - 4*a*c;
    if(Discriminant > 0){
        Xone = ((-1)*b + mySqrt(Discriminant))/(2*a);
        Xtwo = ((-1)*b - mySqrt(Discriminant))/(2*a);
    }else if(Discriminant == 0.0){
        Xone = ((-1)*b)/2*a;
        Xtwo = NULL;
    }else{
        Xone = NULL;
        Xtwo = NULL;
    }
    double * result = new double[3];
    result[0] = Discriminant;
    result[1] = Xone;
    result[2] = Xtwo;
    return result;
}
